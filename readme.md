

# tilem

````
 * TI-73 / TI-73 Explorer
 * TI-76.fr
 * TI-81
 * TI-82
 * TI-82 STATS / TI-82 STATS.fr
 * TI-83
 * TI-83 Plus / TI-83 Plus Silver Edition / TI-83 Plus.fr
 * TI-84 Plus / TI-84 Plus Silver Edition / TI-84 pocket.fr
 * TI-85
 * TI-86
 ````

To have Fraction: 

Press [ALPHA]  then Key [Y=]  (F1 key), and fraction will be [1]. 


 
![](media/tilem.png)

 
![](media/tilem2.png)


![](medias/ti76.png)
![](medias/ti81.png)
![](medias/ti82-stats.png)
![](medias/ti83.png)
![](medias/ti83p.png)
![](medias/ti84p.png)
![](medias/ti86.png)

## Windows 

![](https://gitlab.com/openbsd98324/tilem/-/raw/main/windows-32/tilem2-win7-win32-ti-calc-windows-1.jpg)

## Graph 

Zoom, and graph. 

![](medias/graph/1724761302-screenshot.png) 


